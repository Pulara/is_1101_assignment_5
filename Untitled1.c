#include<stdio.h>

#define currentPrice 15
#define currentAttendance 120
#define performanceCost 500
#define perAttendeeCost 3

//function to calculate the attendance
int	calNoOfAttendees (char type, int n)
{
    int noOfAttendees = currentAttendance;
    switch(type)
    {
    case '+':
        noOfAttendees -= (n/5)*20;
        break;
    case '-' :
        noOfAttendees += (n/5)*20;

    }

    return noOfAttendees;
}
//function to calculate total income
int calIncome(int n, int noOfAttendees)
{
    int income = n*noOfAttendees;
    return income;

}
//function to calculate total cost
int calCost(int noOfAttendees)
{
    int cost= noOfAttendees*perAttendeeCost + performanceCost;
    return cost;
}

/*the main function
 *to display the relationship
  *between the ticket price and the profit*/
int main()
{

    printf("At the current price: \n");
    int profit = calIncome(currentPrice,currentAttendance)-calCost(currentAttendance);
    printf("Ticket price = Rs.%d \t\t Profit = Rs.%d\n\n",currentPrice,profit);
    printf("When decreasing the price: \n");
    char type = '-';
    int d = 5;
    while(profit>0)
    {
        int attendance = calNoOfAttendees(type,d);
        profit = calIncome(currentPrice-d,attendance)-calCost(attendance) ;
        printf("Ticket price = Rs.%d \t\t Profit = Rs.%d\n",(currentPrice-d),profit);
        d+=5;
    }
    printf("\n");
    profit = 1;
    printf("When increasing the price: \n");
    type = '+';
    int i = 5;
    while(profit>0)
    {
        int attendance = calNoOfAttendees(type,i);
        profit = calIncome(currentPrice+i,attendance)-calCost(attendance);
        printf("Ticket price = Rs.%d \t\t Profit = Rs.%d\n",(currentPrice+i),profit);
        i+=5;
    }

    return 0;
}
